# frozen_string_literal: true

control 'alacritty-package-install-pkg-installed' do
  title 'should be installed'

  describe package('alacritty') do
    it { should be_installed }
  end
end
