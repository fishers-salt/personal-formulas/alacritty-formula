# frozen_string_literal: true

control 'alacritty-config-file-user-auser-present' do
  title 'should be present'

  describe user('auser') do
    it { should exist }
  end
end

control 'alacritty-config-file-alacritty-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/alacritty') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'alacritty-config-file-alacritty-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/alacritty/alacritty.yml') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') do
      should include('- /home/auser/.config/alacritty/gruvbox-dark.yml')
    end
  end
end

control 'alacritty-config-file-alacritty-theme-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/alacritty/gruvbox-dark.yml') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('# Colors (Gruvbox dark)') }
  end
end

control 'alacritty-config-file-zsh-env-include-dir-auser-managed' do
  title 'should exist'

  describe directory('/home/auser/.config/zsh/env_includes') do
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0755' }
  end
end

control 'alacritty-config-file-zsh-env-includes-config-auser-managed' do
  title 'should match desired lines'

  describe file('/home/auser/.config/zsh/env_includes/terminal.zsh') do
    it { should be_file }
    it { should be_owned_by 'auser' }
    it { should be_grouped_into 'auser' }
    its('mode') { should cmp '0644' }
    its('content') { should include('#  Your changes will be overwritten.') }
    its('content') { should include('TERMINAL=$(which alacritty)') }
    its('content') { should include('export TERMINAL') }
  end
end
