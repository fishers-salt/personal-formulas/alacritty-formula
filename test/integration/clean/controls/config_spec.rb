# frozen_string_literal: true

control 'alacritty-config-clean-alacritty-theme-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/alacritty/gruvbox-dark.yml') do
    it { should_not exist }
  end
end

control 'alacritty-config-clean-alacritty-config-auser-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/alacritty/alacritty.yml') do
    it { should_not exist }
  end
end

control 'alacritty-config-clean-alacritty-dir-auser-absent' do
  title 'should be absent'

  describe directory('/home/auser/.config/alacritty') do
    it { should_not exist }
  end
end

control 'alacritty-config-clean-zsh-env-includes-config-{{ name }}-absent' do
  title 'should be absent'

  describe file('/home/auser/.config/zsh/env_includes/terminal.zsh') do
    it { should_not exist }
  end
end
