# frozen_string_literal: true

control 'alacritty-package-clean-pkg-absent' do
  title 'should not be installed'

  describe package('alacritty') do
    it { should_not be_installed }
  end
end
