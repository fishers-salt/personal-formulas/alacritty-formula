# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import mapdata as alacritty with context %}

alacritty-package-install-pkg-installed:
  pkg.installed:
    - name: {{ alacritty.pkg.name }}
