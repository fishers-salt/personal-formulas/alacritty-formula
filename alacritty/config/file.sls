# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- set sls_package_install = tplroot ~ '.package.install' %}
{%- from tplroot ~ "/map.jinja" import mapdata as alacritty with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

include:
  - {{ sls_package_install }}

{% if salt['pillar.get']('alacritty-formula:use_users_formula', False) %}

{% for name, user in pillar.get('users', {}).items() if user.absent is not defined or not user.absent %}
{%- set current = salt.user.info(name) -%}
{%- if user == None -%}
{%- set user = {} -%}
{%- endif -%}
{%- set home = user.get('home', current.get('home', "/home/%s" % name)) -%}
{%- set manage = user.get('manage_alacritty', False) -%}
{%- if 'prime_group' in user and 'name' in user['prime_group'] %}
{%- set user_group = user.prime_group.name -%}
{%- else -%}
{%- set user_group = name -%}
{%- endif %}
{%- if manage -%}

alacritty-config-file-user-{{ name }}-present:
  user.present:
    - name: {{ name }}

alacritty-config-file-alacritty-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/alacritty
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - alacritty-config-file-user-{{ name }}-present

alacritty-config-file-alacritty-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/alacritty/alacritty.yml
    - source: {{ files_switch([
                  name ~ '-alacritty.yml.tmpl',
                  'alacritty.yml.tmpl'],
                lookup='alacritty-config-file-alacritty-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - context:
        home: {{ home }}
    - require:
      - alacritty-config-file-alacritty-dir-{{ name }}-managed

alacritty-config-file-alacritty-theme-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/alacritty/gruvbox-dark.yml
    - source: {{ files_switch([
                  name ~ '-gruvbox-dark.yml.tmpl',
                  'gruvbox-dark.yml.tmpl'],
                lookup='alacritty-config-file-alacritty-theme-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - alacritty-config-file-alacritty-dir-{{ name }}-managed

{%- set preferred_terminal = user.get('preferred_terminal', none) %}
{%- if preferred_terminal == 'alacritty' %}
alacritty-config-file-zsh-env-include-dir-{{ name }}-managed:
  file.directory:
    - name: {{ home }}/.config/zsh/env_includes
    - user: {{ name }}
    - group: {{ user_group }}
    - mode: '0755'
    - makedirs: True
    - require:
      - alacritty-config-file-user-{{ name }}-present

alacritty-config-file-zsh-env-includes-config-{{ name }}-managed:
  file.managed:
    - name: {{ home }}/.config/zsh/env_includes/terminal.zsh
    - source: {{ files_switch([
                  name ~ '-alacritty_env.zsh.tmpl',
                  'alacritty_env.zsh.tmpl'],
                lookup='alacritty-config-file-zsh-env-includes-config-' ~ name ~ '-managed',
                )
              }}
    - mode: '0644'
    - user: {{ name }}
    - group: {{ user_group }}
    - template: jinja
    - require:
      - alacritty-config-file-zsh-env-include-dir-{{ name }}-managed
{%- endif %}

{% endif %}
{% endfor %}
{% endif %}
